import './style.css'

const ul = document.querySelector('ul')
const form = document.querySelector('form')
const input = document.querySelector('form > input')

input.focus()

form.addEventListener('submit', event => {
  event.preventDefault()
  const value = input.value
  if (!value) {
    alert('Veuillez remplir le champ')
    return false
  }
  input.value = ''
  addTodo(value)
  input.focus()
})

const todos = [
  {
    text: 'Je suis une todo',
    done: false,
    editMode: false
  },
  {
    text: 'Faire du JavaScript',
    done: true,
    editMode: false
  }
]

const displayTodo = () => {
  const todosNode = todos.map((todo, index) => {
    if (todo.editMode) {
      return createTodoEditElement(todo, index)
    } else {
      return createTodoElement(todo, index)
    }
  })
  ul.innerHTML = ''
  ul.append(...todosNode)
}

const createTodoElement = (todo, index) => {
  const li = document.createElement('li')
  const buttonDelete = document.createElement('button')
  const buttonEdit = document.createElement('button')
  buttonEdit.innerHTML = 'Edit'
  buttonEdit.className = 'edit'
  buttonDelete.innerHTML = 'Supprimer'
  buttonDelete.className = 'delete'
  buttonDelete.addEventListener('click', event => {
    event.stopPropagation()
    deleteTodo(index)
  })
  buttonEdit.addEventListener('click', event => {
    event.stopPropagation()
    toggleEditMode(index)
  })
  li.innerHTML = `
    <span class='todo ${todo.done ? 'done' : ''}'></span>
    <p class='${todo.done ? 'done' : ''}'>${todo.text}</p>
  `
  let timer
  li.addEventListener('click', event => {
    if (event.detail === 1) {
      timer = setTimeout(() => {
        toggleTodo(index)
      }, 200)
    } else if (event.detail === 2) {
      toggleEditMode(index)
      clearTimeout(timer)
    }
  })
  li.append(buttonEdit, buttonDelete)
  return li
}

const createTodoEditElement = (todo, index) => {
  const li = document.createElement('li')
  const input = document.createElement('input')
  const buttonSave = document.createElement('button')
  const buttonCancel = document.createElement('button')
  input.type = 'text'
  input.value = todo.text
  input.addEventListener('keydown', event => {
    if (event.key === 'Enter') {
      saveEdit(index, input)
    } else if (event.key === 'Escape') {
      toggleEditMode(index)
    }
  })
  buttonSave.innerHTML = 'Save'
  buttonCancel.innerHTML = 'Cancel'
  buttonCancel.addEventListener('click', event => {
    event.stopPropagation()
    toggleEditMode(index)
  })
  buttonSave.addEventListener('click', event => {
    event.stopPropagation()
    saveEdit(index, input)
  })
  li.append(input, buttonCancel, buttonSave)
  return li
}

const addTodo = text => {
  todos.push({
    text: `${text[0].toUpperCase()}${text.slice(1)}`,
    done: false
  })
  displayTodo()
}

const deleteTodo = index => {
  todos.splice(index, 1)
  displayTodo()
}

const toggleTodo = index => {
  todos[index].done = !todos[index].done
  displayTodo()
}

const toggleEditMode = index => {
  todos[index].editMode = !todos[index].editMode
  displayTodo()
}

const saveEdit = (index, input) => {
  const value = input.value
  todos[index].text = value
  todos[index].editMode = false
  displayTodo()
}

displayTodo()
